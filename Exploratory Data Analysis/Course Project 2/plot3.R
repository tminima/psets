library(ggplot2)

# Read the dataset
NEI <- readRDS("data/summarySCC_PM25.rds")
SCC <- readRDS("data/Source_Classification_Code.rds")
NEI <- transform(NEI, type = factor(type), year = factor(year))

# Creation of the matrix of total emissions of the baltimore city
baltimore <- subset(NEI, fips == "24510")
baltimore_sources <-  aggregate(Emissions ~ type + year, data = baltimore, sum)

png("plot3.png", width = 480, height = 480, units = "px")
g <- ggplot(baltimore_sources, aes(x = year, y = Emissions, color = type)) +
    geom_point() +
    geom_line(aes(group = type)) +
    ylim(0, 2500) +
    theme_bw() +
    scale_colour_discrete(name = "Source Type") +
    labs(x = "",
         y = "PM2.5 Emissions (tons)",
         title = "Yearly PM2.5 Emissions in Baltimore City")
print(g)
dev.off()