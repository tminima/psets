/***************************************************************************
* fifteen.c
*
* Computer Science 50
* Problem Set 3
*
* Implements The Game of Fifteen (generalized to d x d).
*
* Usage: fifteen d
*
* whereby the board's dimensions are to be d x d,
* where d must be in [DIM_MIN,DIM_MAX]
*
* Note that usleep is obsolete, but it offers more granularity than
* sleep and is simpler to use than nanosleep; `man usleep` for more.
***************************************************************************/

#define _XOPEN_SOURCE 500


//#include "Astar.c"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include "cs50.h"

// constants
#define DIM_MIN 3
#define DIM_MAX 9


// board
int board[DIM_MAX][DIM_MAX];

// dimensions
int d;


// prototypes
void clear(void);
void greet(void);
void init(void);
void draw(void);
bool cheat(void);
bool move(int tile);
bool won(void);


int
main(int argc, char *argv[])
{
        // greet user with instructions
        greet();

        // ensure proper usage
        if (argc != 2)
        {
                printf("Usage: fifteen d\n");
                return 1;
        }

        // ensure valid dimensions
        d = atoi(argv[1]);
        if (d < DIM_MIN || d > DIM_MAX)
        {
                printf("Board must be between %d x %d and %d x %d, inclusive.\n",
                DIM_MIN, DIM_MIN, DIM_MAX, DIM_MAX);
                return 2;
        }

        // initialize the board
        init();

        // accept moves until game is won
        while (true)
        {
                // clear the screen
                clear();

                // draw the current state of the board
                draw();

                // check for win
                if (won())
                {
                        printf("ftw!\n");
                        break;
                }

                // prompt for move
                printf("Tile to move: ");
                string input = GetString();
                int i=0, tile;
                while(input[i] != '\0')
                {
                        if(isdigit(input[i]) != 0)
                                i++;
                        else
                                break;
                }

                if(i == strlen(input))
                {
                        tile = atoi(input);
                        // move if possible, else report illegality
                        if (!move(tile))
                        {
                                printf("\nIllegal move.\n");
                                usleep(500000);
                                continue;
                        }
                }
                else
                {
                        if(strcmp(input, "GOD") == 0)
                        {
                                cheat();
                                printf("GOD mode activated!\n");
                                break;
                        }
                        else
                        {
                                printf("\nIllegal move.\n");
                                usleep(500000);
                                continue;
                        }
                }
                // sleep thread for animation's sake
                usleep(500000);
        }

        // that's all folks
        return 0;
}


/*
* Clears screen using ANSI escape sequences.
*/

void
clear(void)
{
        printf("\033[2J");
        printf("\033[%d;%dH", 0, 0);
}


/*
* Greets player.
*/

void
greet(void)
{
        clear();
        printf("WELCOME TO THE GAME OF FIFTEEN\n");
        usleep(2000000);
}


/*
* Initializes the game's board with tiles numbered 1 through d*d - 1
* (i.e., fills 2D array with values but does not actually print them).  
*/

void
init(void)
{
        int i=0, j=0, init, a=0, k, l;
        srand((unsigned int) time(NULL));
        for(i=d-1; i>=0; i--)
        {
                for(j=d-1; j>=0; j--)
                {
                        board[i][j] = a;
                        a++;
                }
        }
        if(d%2 == 0)
        {
                a = board[d-1][d-2];
                board[d-1][d-2] = board[d-1][d-3];
                board[d-1][d-3] = a;
        }

        i=d-1;
        j=d-1;
        k=0;
        l=0;
        while(1)
        {

                init = rand() % 2;
                if(k<d-1)
                {
                        if(init == 1 && i>0)
                        {
                                i = i-1;
                                board[i+1][j] = board[i][j];
                                board[i][j] = 0;
                                k++;
                        }
                }
                else
                {
                        if(init == 1 && i<d-1)
                        {
                                i = i+1;
                                board[i-1][j] = board[i][j];
                                board[i][j] = 0;
                        }
                }	
                if(l<d-1)
                {
                        if(init == 0 && j>0)
                        {
                                j = j-1;
                                board[i][j+1] = board[i][j];
                                board[i][j] = 0;
                                l++;
                        }
                }
                else
                {
                        if(init == 0 && j<d-1)
                        {
                                j = j+1;
                                board[i][j-1] = board[i][j];	
                                board[i][j] = 0;
                        }
                }
                if((i == d-1) && (j == d-1))
                        break;
        }
}



/* 
* Prints the board in its current state.
*/

void
draw(void)
{
        int i, j;
        for(i=0; i<d; i++)
        {
                for(j=0; j<d; j++)
                {
                // if(i==d && j==d)
                // 	printf("0");
                // else
                printf("%2d\t", board[i][j]);		//"%2d" will print blank spaces before no if it is less than 2 digits.
                }
                printf("\n\n");
        }
}


/* 
* If tile borders empty space, moves tile and returns true, else
* returns false. 
*/

bool
move(int tile)
        {
        int i, j;
        if(tile <= d*d-1 && tile >= 1)
        {
                for(i=0; i<d; i++)
                {
                        for(j=0; j<d; j++)
                        {
                                if(tile == board[i][j])
                                {
                                        if(board[i][j+1] == 0 && j<d-1)
                                        {
                                                board[i][j+1] = tile;
                                                board[i][j] = 0;
                                                return true;
                                        }
                                        else if(board[i][j-1] == 0 && j>0)
                                        {
                                                board[i][j-1] = tile;
                                                board[i][j] = 0;
                                                return true;
                                        }
                                        else if(board[i+1][j] == 0 && i<d-1)
                                        {
                                                board[i+1][j] = tile;
                                                board[i][j] = 0;
                                                return true;
                                        }
                                        else if(board[i-1][j] == 0 && i>0)
                                        {
                                                board[i-1][j] = tile;
                                                board[i][j] = 0;
                                                return true;
                                        }
                                        else
                                        return false;
                                }
                        }
                }
        }
        return false;
}


/*
* Returns true if game is won (i.e., board is in winning configuration), 
* else false.
*/

bool
won(void)
{
        int i, j;
        if(d%2!=0)
        {
                for(i=0; i<d; i++)
                {
                        for(j=0; j<d-1; j++)
                        {
                                if(board[i][j]>board[i][j+1])
                                return false;
                        }
                }
        }
        else
        {
                for(i=0; i<d; i++)
                {
                        for(j=0; j<d-1; j++)
                        {
                                if(i==d-1 && j>d-4)
                                        break;
                                if(board[i][j]>board[i][j+1])
                                        return false;
                        }
                }                  
                if(board[d-1][d-2] != 2 || board[d-1][d-3] != 1)
                        return false;
        }
        for(i=0; i<d; i++)
        {
                for(j=0; j<d-1; j++)
                {
                        if(board[j][i]>board[j+1][i])
                        return false;
                }
        }   
        return true;
}

/*
* Activates cheat mode.
*/

bool
cheat(void)
{

        return false;

}



