/*
* Write, in a file called mario.c in your ~/Dropbox/hacker1 directory, a program that recreates these half-pyramids using hashes (#) for blocks. 
* However, to make things more interesting, first prompt the user for the half-pyramids' heights, a non-negative integer no greater than 23. 
* (The height of the half-pyramids pictured above happens to be 4, the width of each half-pyramid 4, with an a gap of size 2 separating them.) 
* Then, generate (with the help of printf and one or more loops) the desired half-pyramids. Take care to left-align the bottom-left corner of the 
* left-hand half-pyramid, as in the sample output below, wherein boldfaced text represents some user’s input.
*/

# include <stdio.h>

void
space( int a )
{
        int i;
        for(i=0; i<a; i++)
                printf(" ");
}

void
block( int a )
{
        int i;
        for(i=0; i<a; i++)
                printf("#");
}

int
main()
{
        int height, i;
        printf("Enter the height : ");
        scanf("%d", &height);
        while(height > 23 || height < 1)
        {
                printf("Retry : ");
                scanf("%d", &height);
        }
        for(i=0; i<height; i++)
        {
                space(height-i-1);
                block(i+1);
                printf("  ");
                block(i+1);
                printf("\n");
        }
        return 0;
}