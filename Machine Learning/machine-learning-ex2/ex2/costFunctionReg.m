function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

% Vectorized cost function
h_theta = sigmoid(X*theta);

log_h_theta = log(h_theta);
log_1_minus_h_theta = log(1 - h_theta);

j_theta_part1 = -1 .* y .* log_h_theta;
j_theta_part2 = -1 .* (1 - y) .* log_1_minus_h_theta;

regularization_part = (lambda / (2 * m)) * sum(theta(2:end) .^ 2);

J = (sum(j_theta_part1 + j_theta_part2) / m) + regularization_part;

% Vectorized gradient function
grad(1) = sum((h_theta - y) .* X(:, 1)) / m;
grad(2:end) = (sum((h_theta - y) .* X(:, 2:end)) / m) .+ ((lambda/m) .* theta(2:end))';


% =============================================================

end
