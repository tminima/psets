function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the
%   cost of using theta as the parameter for linear regression to fit the
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%

% Cost function
h_theta = X * theta;
non_reg_part = (h_theta - y) .^ 2;
non_reg_part = sum(non_reg_part);

reg_part = theta(2: end) .^ 2;
reg_part = lambda * sum(reg_part);

J = (non_reg_part + reg_part) / (2 * m);

% Gradients
grad = ((h_theta - y)' * X)';
% size(grad)
grad(2 : end) = grad(2 : end) .+ (lambda .* theta(2 : end));
grad = grad ./ m;

% =========================================================================

grad = grad(:);

end
