/**
 * copy.c
 *
 * Computer Science 50
 * Problem Set 5
 *
 * Copies a BMP piece by piece, just because.
 */
       
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"

int main(int argc, char* argv[])
{
    // ensure proper usage
    if (argc != 4)
    {
        printf("Usage: ./copy f infile outfile\n");
        return 1;
    }

    int f = atoi(argv[1]);

    // remember filenames
    char* infile = argv[2];
    char* outfile = argv[3];

    // open input file 
    FILE* inptr = fopen(infile, "r");
    if (inptr == NULL)
    {
        printf("Could not open %s.\n", infile);
        return 2;
    }

    // open output file
    FILE* outptr = fopen(outfile, "w");
    if (outptr == NULL)
    {
        fclose(inptr);
        fprintf(stderr, "Could not create %s.\n", outfile);
        return 3;
    }

    // read infile's BITMAPFILEHEADER
    BITMAPFILEHEADER bf;
    fread(&bf, sizeof(BITMAPFILEHEADER), 1, inptr);

    // read infile's BITMAPINFOHEADER
    BITMAPINFOHEADER bi;
    fread(&bi, sizeof(BITMAPINFOHEADER), 1, inptr);

    // printf("%d", bf.bfSize);
    // ensure infile is (likely) a 24-bit uncompressed BMP 4.0
    if (bf.bfType != 0x4d42 || bf.bfOffBits != 54 || bi.biSize != 40 || 
        bi.biBitCount != 24 || bi.biCompression != 0)
    {
        fclose(outptr);
        fclose(inptr);
        fprintf(stderr, "Unsupported file format.\n");
        return 4;
    }

    int padding =  (4 - (bi.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;
    int newpad = (padding * f) % 4;
    bi.biSizeImage = (bi.biSizeImage * f - padding * abs(bi.biHeight) * f + newpad * abs(bi.biHeight)) * f;
    bf.bfSize = ((bf.bfSize - bf.bfOffBits - padding * abs(bi.biHeight)) * f * f + newpad * abs(bi.biHeight) * f ) + bf.bfOffBits;
    int biHeight = abs(bi.biHeight);
    bi.biHeight *= f;
    int oldWid = bi.biWidth;
    bi.biWidth *= f;

    // write outfile's BITMAPFILEHEADER
    fwrite(&bf, sizeof(BITMAPFILEHEADER), 1, outptr);

    // write outfile's BITMAPINFOHEADER
    fwrite(&bi, sizeof(BITMAPINFOHEADER), 1, outptr);

    RGBTRIPLE line[bi.biWidth];
    int s=0;
    // iterate over infile's scanlines
    for (int i = 0; i < biHeight; i++)
    {
        // iterate over pixels in scanline
        s=0;
        for (int j = 0; j < oldWid; j++)
        {
            fread(&line[s], sizeof(RGBTRIPLE), 1, inptr);
            s++;
            for(int k=1; k< f; k++, s++)
                line[s] = line[s-1];
            // write RGB triple to outfile
            // for(int k=1; k< f; k++)
            //     fwrite(&triple, sizeof(RGBTRIPLE), 1, outptr);
        }
        for(int k=0; k<f; k++)
        {
            for(int j=0; j<bi.biWidth; j++)
                fwrite(&line[j], sizeof(RGBTRIPLE), 1, outptr);
            for (int k = 0; k < newpad; k++)
                fputc(0x00, outptr);
        }
        fseek(inptr, padding, SEEK_CUR);

        // skip over padding, if any

        // // then add it back (to demonstrate how)
        // for (int k = 0; k < (padding*f)%4; k++)
        // {
        //     fputc(0x00, outptr);
        // }
    }

    // close infile
    fclose(inptr);

    // close outfile
    fclose(outptr);

    // that's all folks
    return 0;
}
