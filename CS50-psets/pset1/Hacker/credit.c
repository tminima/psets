/*
* In credit.c, write a program that prompts the user for a credit card number and then reports (via printf) whether it is a valid 
* American Express, MasterCard, or Visa card number, per the definitions of each’s format herein. So that we can automate 
* some tests of your code, we ask that your program’s last line of output be AMEX\n or MASTERCARD\n or VISA\n or INVALID\n, 
* nothing more, nothing less, and that main always return 0. For simplicity, you may assume that the user’s input will be entirely 
* numeric (i.e., devoid of hyphens, as might be printed on an actual card). But do not assume that the user’s input will fit in an int!
*/

# include <stdio.h>
# include <string.h>

int len=0, crd[16];
char num[16];

int convert()
{
	int j,c=0;
	for(j=0; j<strlen(num); j++)
	{
		crd[j]=num[j]-48;
		// printf("%d-", crd[j]);
	}
	len=j;
	for(j=0; j<len; j++)
	{
		if(crd[j]<10 && crd[j]>=0)
			c++;
		else
			return 0;
	}
	return c;
}

int check()
{
	int j, sum=0;
	for(j=0; j<len; j++)
	{
		if(j%2 == 0)
			sum+=crd[j];
		else
			sum+= ((crd[j]*2)%10)+((crd[j]*2)/10);
		// printf("%d\n", sum);
	}
	// printf("%d\n", sum);
	if(sum%10 == 0 )
		return 0;
	else
		return 1; 
}



int main()
{
	int b;
	printf("Enter your credit card no. : ");
	scanf("%s", num);
	while(convert() != len)
	{
		printf("Retry : ");
		scanf("%s", num);
	}
	if(check()==0)
	{
		if(crd[0]==3 && len==15 && (crd[1]==4 || crd[1]==7))
			printf("AMEX\n");
		else if(crd[0]==5 && len==16 && (crd[1]==1 || crd[1]==2 || crd[1]==3 || crd[1]==4 || crd[1]==5))
			printf("MASTERCARD\n");
		else if(crd[0]==4 && (len==16 || len==13))
			printf("VISA\n");
		else
			printf("Don't know!\n");
	}
	else
		printf("INVALID\n");
	return 0;
}


