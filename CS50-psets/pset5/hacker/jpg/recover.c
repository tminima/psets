/**
 * recover.c
 *
 * Computer Science 50
 * Problem Set 5
 *
 * Recovers JPEGs from a forensic image.
 */

# include <stdio.h>
# include <string.h>
#include <stdint.h>

// # include "jpeg.h"

typedef uint8_t BYTE;

int check(BYTE one, BYTE two, BYTE three, BYTE four)
{
        if(one == 0xff && two == 0xd8 && three == 0xff && (four == 0xe0 || four == 0xe1))
                return 0;
        return 1;
}

int main()
{
        // TODO
        FILE* rawFile = fopen("card.raw", "r");
        char title[8] = "000.jpg";
        int i = 0;
        if (rawFile == NULL)
        {
                printf("Could not open 'card.raw'.\n");
                return 2;
        }
        FILE* img = fopen(title, "w");
        BYTE one, two, three, four;
        BYTE inp[512];
        memset(inp, 0x00, 512*sizeof(BYTE));
        fread(&one, sizeof(BYTE), 1, rawFile);
        fread(&two, sizeof(BYTE), 1, rawFile);
        fread(&three, sizeof(BYTE), 1, rawFile);
        fread(&four, sizeof(BYTE), 1, rawFile);
        fseek(rawFile, -4, SEEK_CUR);
        while(1)
        {
                fread(&inp, sizeof(BYTE), 512, rawFile);
                if(check(one, two, three, four) == 0)
                {
                        fclose(img);
                        sprintf(title, "%03d.jpg", i);
                        img = fopen(title, "w");
                        i++;
                }
                fwrite(&inp, sizeof(BYTE), 512, img);
                fread(&one, sizeof(BYTE), 1, rawFile);
                fread(&two, sizeof(BYTE), 1, rawFile);
                fread(&three, sizeof(BYTE), 1, rawFile);
                fread(&four, sizeof(BYTE), 1, rawFile);
                fseek(rawFile, -4, SEEK_CUR);
                if(feof(rawFile))
                {
                        fclose(img);
                        break;
                }
        }
        return 0;
}