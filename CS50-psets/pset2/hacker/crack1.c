# define _XOPEN_SOURCE
# include <unistd.h>
# include <stdio.h>
# include <string.h>
# include <strings.h>
# include <stdlib.h>

int wordSize = 9;
int lineSize = 30;


int
check(char *dictWord, char *salt, char *hash)
{
        if(strcmp( crypt( dictWord , salt) , hash ) == 0 )
        {
                printf( "%s\n" , dictWord );
                return 0;
        }
        return 1;
}

int
dictionary(char *salt, char *hash)
{
        int i;
        char str[lineSize], word[wordSize];
        FILE *f;
        f = fopen( "words" , "r" );
        if(!f)
        {
                printf("Error opening file!\n");
                exit(1);
        }
        while(!feof(f))
        {

                fgets(str, 50, f);
                str[strlen(str)-1] = '\0';

                if(check(str, salt, hash) == 1)
                        continue;
                else
                {
                        fclose(f);
                        return 0;
                }
        }
        fclose(f);
        return 1;
}

// int brute(char *salt, char *hash)
// {
//         char chars[] = {"0123456789abcdefghijklmnopqrstuvwxyz"};
//         //char chars[] = {"0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz !\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~"};
//         int length = strlen(chars), max_chars, loop_over, i, shift, curr_char;
//         char cur_word[wordSize];
//         int pos[wordSize-1];
//         cur_word[wordSize] = '\0';

//         // Outer loop - controls word length
//         for (max_chars = 7; max_chars >= 0; max_chars--)
//         {
//                 printf("Trying length %d\n", max_chars + 1);
//                 loop_over = 0;

//                 // Initialize the word
//                 for (i = 0; i <= max_chars; i++)
//                 {
//                         pos[i] = 0;
//                         cur_word[i] = chars[pos[i]];
//                 }
//                 cur_word[i] = '\0';

//                 // Main brute-force loop
//                 // Ends when every symbol combination is checked
//                 while (!loop_over)
//                 {
//                         // Try currend word
//                         if (check(cur_word, salt, hash) == 0)
//                                 return 0;

//                         // One iteration of brute-force
//                         shift = 1;

//                         for (curr_char = 0; curr_char <= max_chars; curr_char++)
//                         {
//                                 // If shift - change character
//                                 if (shift)
//                                 {
//                                         // If all character are done
//                                         if (pos[curr_char] == length - 1)
//                                         {
//                                                 // If last symbol - loop is over
//                                                 if (curr_char == max_chars)
//                                                         loop_over = 1;
//                                                 // Change to first symbol again
//                                                 else
//                                                 {
//                                                         pos[curr_char] = 0;
//                                                         cur_word[curr_char] = chars[pos[curr_char]];
//                                                 }
//                                         }
//                                         else
//                                         {
//                                                 cur_word[curr_char] = chars[++pos[curr_char]];
//                                                 shift = 0;
//                                         }
//                                 }
//                                 printf("%s\n", cur_word);
//                                 // sleep(1);
//                         }
//                 }
//         }
//         return 1;
}

int main( int argc , char *argv[] )
{
        char salt[3];
        if(argc != 2 || strlen(argv[1]) != 13)
        {
                printf( "bad input !!\n" );
                return 1;
        }
        salt[0]=argv[1][0];
        salt[1]=argv[1][1];
        salt[2]='\0';

        if(dictionary(salt, argv[1]) == 0)
                return 0;
        else if(brute(salt, argv[1]) == 0)
                return 0;
        else
                printf("Cannot be cracked by me!!\n");
        return 0;
}