# Number of overlapping substrings of "TGT" in a.
# count
start = count = 0
while 1:
    start = a.find("TGT", start) + 1
    if start:
        count += 1
    else:
        break

print(count)


# Find the 3-mer in the fragment.
for i in range(len(a)):
    start = count = 0
    sub = a[i:i + 3]
    while 1:
        start = a.find(sub, start) + 1
        if start:
            count += 1
        else:
            break
    if count > 2 and len(sub) == 3:
        print(sub, count)


# Skew
a = "CATTCCAGTACTTCGATGATGGCGTGAAGA"
g = c = 0
for i, j in enumerate(a):
    total = 0
    if j == "G":
        g += 1
    elif j == "C":
        c += 1
    total = g - c
    print(i, total)


# Hamming distance
a = "CTTGAAGTGGACCTCTAGTTCCTCTACAAAGAACAGGTTGACCTGTCGCGAAG"
b = "ATGCCTTACCTAGATGCAATGACGGACGTATTCCTTTTGCCTCAACGGCTCCT"

print(sum(ch1 != ch2 for ch1, ch2 in zip(a, b)))


# count1
count = 0
a = "CATGCCATTCGCATTGTCCCAGTGA"
for i in range(len(a)):
    sub = a[i:i + 3]
    if len(sub) == 3:
        if sum(ch1 != ch2 for ch1, ch2 in zip(sub, "CCC")) < 2:
            count += 1
        print(sub, "CCC", count)

print(count)


# Neighborhood
a = "CATGCCATTCGCATTGTCCCAGTGA"

count = 0
for i in range(len(a)):
    sub = a[i:i + 4]
    if len(sub) == 4:
        if sum(ch1 != ch2 for ch1, ch2 in zip(sub, "TAGC")) < 4:
            count += 1
        print(sub, "TAGC", count)

print(count)
