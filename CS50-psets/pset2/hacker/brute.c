 int brute(char *passwd, char *salt)
{
        char chars[] = {"0123456789abcdefghijklmnopqrstuvwxyz"};
        //char chars[] = {"0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz !\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~"};
        int length = strlen(chars), max_chars, loop_over, i, shift, curr_char;
        char cur_word[wordSize];
        int pos[wordSize-1];
        cur_word[wordSize] = '\0';

        // Outer loop - controls word length
        for (max_chars = 7; max_chars >= 0; max_chars--)
        {
                printf("Trying length %d\n", max_chars + 1);
                loop_over = 0;

                // Initialize the word
                for (i = 0; i <= max_chars; i++)
                {
                        pos[i] = 0;
                        cur_word[i] = chars[pos[i]];
                }
                cur_word[i] = '\0';

                // Main brute-force loop
                // Ends when every symbol combination is checked
                while (!loop_over)
                {
                        // Try current word
                        if (check(cur_word, salt, hash))
                                return 0;

                        // One iteration of brute-force
                        shift = 1;

                        for (curr_char = 0; curr_char <= max_chars; curr_char++)
                        {
                                // If shift - change character
                                if (shift)
                                {
                                        // If all character are done
                                        if (pos[curr_char] == length - 1)
                                        {
                                                // If last symbol - loop is over
                                                if (curr_char == max_chars)
                                                        loop_over = 1;
                                                // Change to first symbol again
                                                else
                                                {
                                                        pos[curr_char] = 0;
                                                        cur_word[curr_char] = chars[pos[curr_char]];
                                                }
                                        }
                                        else
                                        {
                                                cur_word[curr_char] = chars[++pos[curr_char]];
                                                shift = 0;
                                        }
                                }
                        }
                }
        }
        return 1;
}