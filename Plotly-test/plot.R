library(plotly)

dataset <- read.csv("Data/literacy_rate.csv")
dataset$Year = as.factor(dataset$Year)
dataset = subset(dataset, Year == 1951 | Year == 1981 | Year == 2011)

# Stacked Bar Chart
png("Plots/plot2.png", width=1366, height=768, res=120)
b <- ggplot(dataset, aes(x=State.UTs, y=Literacy.Rate, fill=Year)) + geom_bar(stat="identity", colour="black") +
    scale_fill_hue(l=40) +
    theme(axis.text.x=element_text(angle=70, hjust=1)) +
    labs(x=" ", y="Literacy Rate") +
    ggtitle("All India Literacy Rate (State-wise)")
dev.off()


# Stacked Bar Chart with different colors and transparency
png("Plots/plot3.png", width=1366, height=768, res=120)
a <- ggplot(dataset, aes(x=State.UTs, y=Literacy.Rate, fill=Year)) + geom_bar(stat="identity", colour="black", alpha=0.4) +
    theme(axis.text.x=element_text(angle=70, hjust=1), legend.position=c(0.95,0.85)) +
    labs(x=" ", y="Literacy Rate") +
    scale_fill_brewer(palette="Spectral") +
    ggtitle("All India Literacy Rate (State-wise)")
dev.off()


# Side-by-side Bar Chart
png("Plots/plot1.png", width=1366, height=768, res=120)
c <- ggplot(dataset, aes(x=State.UTs, y=Literacy.Rate, fill=Year)) + geom_bar(stat="identity", position=position_dodge(), colour="black") +
    scale_fill_hue(l=40) +
    theme(axis.text.x=element_text(angle=70, hjust=1)) +
    labs(x=" ", y="Literacy Rate") +
    ggtitle("All India Literacy Rate (State-wise)")
dev.off()


# Bar Chart with one layer upon the other (without legend)
png("Plots/plot4.png", width=1366, height=768, res=120)
d <- ggplot(dataset, aes(x=State..UTs)) +
    geom_bar(aes(y=X2011, colour="red"), stat="identity", alpha=0.5, color="black") +
    geom_bar(aes(y=X1991, colour="X1991"), stat="identity", alpha=0.5, color="black") +
    geom_bar(aes(y=X1971, colour="X1971"), stat="identity", alpha=0.5, color="black") +
    geom_bar(aes(y=X1951, colour="X1951"), stat="identity", alpha=0.5, color="black") +
    theme(axis.text.x=element_text(angle=90, vjust=0.5, hjust=1))
dev.off()



# Uploading the plots to the plotly
py <- plotly()

py$ggplotly(a)
py$ggplotly(b)
py$ggplotly(c)
py$ggplotly(d)




