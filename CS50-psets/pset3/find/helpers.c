/**************************************************************************** 
 * helpers.c
 *
 * Computer Science 50
 * Problem Set 3
 *
 * Helper functions for Problem Set 3.
 ***************************************************************************/
       
#include "cs50.h"

#include "helpers.h"
#include <stdio.h> 
#include <time.h>
#include <string.h>

const int limit = 65536;

/*
 * Returns true if value is in array of n values, else false.
 */

bool 
search(int value, int array[], int n)
{
	int i, mid=0, last=n-1, first=0;
	for(i=0; i<n; i++)
		printf("%d  ", array[i]);
	printf("\n");
	while(first <= last)
	{
		mid = (first+last)/2;
		if(value == array[mid])
			return true;
		else if(value < array[mid])
			last = mid-1;
		else if(value > array[mid])
			first = mid+1;
	}
	return false;
}



/*
 * Sorts array of n values.  Returns true if
 * successful, else false.
 */
bool
sort(int values[], int n)
{
        int offset[limit], output[n], i, holder, sum=0;
        printf("%d\n", limit);
        memset(offset, 0, limit*sizeof(int));
        memset(offset, 0, limit*sizeof(int));
        for(i=0; i<n; i++)
                offset[values[i]]++;
        printf("sdfsd\n");

        holder = offset[0];
        offset[0] = 0;
        for (i = 1; i < limit; i++)
        {
                sum = offset[i-1] + holder;
                holder = offset[i];
                offset[i] = sum;
        }
        for(i=0; i<n; i++)
        {
                // holder = values[i];
                output[offset[values[i]]++] = values[i];
        }
        for (i = 0; i < n; i++)
                values[i] = output[i];
        return true;
}


// int
// merge(int a[], int b[], int c[], int s);

// bool
// sort(int values[], int n)
// {
// 	int i, j, s1=n/2, s2=n-s1, ar1[s1], ar2[s2];
// 	if(s1>0 && s2>0)
// 	{
// 		for(i=0; i<s1; i++)
// 			ar1[i]=values[i];
// 		for(j=0; j<s2; j++)
// 			ar2[j]=values[j+i];

// 		sort(ar1, s1);
// 		sort(ar2, s2);
// 		merge (ar1, ar2, values, s1+s2);
// 	}
// 	return true;
// }


// int
// merge(int a[], int b[], int c[], int s)
// {
// 	int i=0, j=0, k=0;
	
// 	while(i<s/2 && j<(s-s/2))
// 	{
// 		if(a[i]<=b[j])
// 		{
// 			c[k]=a[i];
// 			i++;
// 		}
// 		else
// 		{
// 			c[k]=b[j];
// 			j++;
// 		}
// 		k++;
// 	}
// 	for(; k<s; k++)
// 	{
// 		if(i==s/2)
// 		{
// 			c[k]=b[j];
// 			j++;
// 		}
// 		else
// 		{
// 			c[k]=a[i];
// 			i++;
// 		}
// 	}
// 	return 0;
// }
