# Read the dataset
NEI <- readRDS("data/summarySCC_PM25.rds")
SCC <- readRDS("data/Source_Classification_Code.rds")
NEI <- transform(NEI, type = factor(type), year = factor(year))

# Extracting the rows containing vehicle, motor or engine words.
vehicular_sources <- data.frame(SCC=SCC$SCC,
    sources = grepl(
        "(veh[i ])|(motor)|(engine)",
        SCC$Short.Name,
        ignore.case = T
    )
)

# Creation of the matrix of total emissions of the baltimore city
baltimore <- subset(NEI, fips == "24510")
baltimore_motor_vehicles <- merge(baltimore, vehicular_sources, by = "SCC")
baltimore_motor_vehicles <- subset(baltimore_motor_vehicles, sources == T)

total_vehicular_emissions <- tapply(baltimore_motor_vehicles$Emissions, baltimore_motor_vehicles$year, sum)

png("plot5.png", width = 480, height = 480, units = "px")
barplot(total_vehicular_emissions,
    ylim = c(0, 700),
    main = "Yearly PM2.5 Vehicular Emissions",
    ylab = "PM2.5 Emissions (tons)")

text(0.7, 643, "633")
text(1.9, 172, "162")
text(3.1, 169, "159")
text(4.3, 105, "95")
dev.off()
